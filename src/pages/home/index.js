import React, { Component } from "react";
import { v4 as uuidv4 } from "uuid";

import Header from "../../presentation/todoHeader";
import Main from "../../presentation/todoList";
import Footer from "../../presentation/todoFooter";
import { setTodos, getTodos } from "../../service";

import "./style.css";

class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: [],
      status: this.props.status,
    };
  }

  saveTodo = (task) => {
    const newTaskId = uuidv4();
    const newTodo = {
      _id: newTaskId,
      task,
      isCompleted: false,
    };
    this.setState((prevState) => {
      return { todos: [newTodo, ...prevState.todos] };
    });
  };

  changeCheckedTodo = (id) => {
    this.setState(({ todos }) => {
      todos
        .filter((todo) => todo._id === id)
        .map((todo) => {
          todo.isCompleted = !todo.isCompleted;
        });
      return todos;
    });
  };

  changeCheckedAllTodos = (checked) => {
    this.setState({
      todos: this.state.todos.map((todo) => {
        todo.isCompleted = !checked;
        return todo;
      }),
    });
  };

  deleteTodo = (id) => {
    this.setState({
      todos: this.state.todos.filter((todo) => todo._id !== id),
    });
  };

  deleteCompleted = () => {
    const notCompleted = this.state.todos.filter((todo) => !todo.isCompleted);
    this.setState({
      todos: notCompleted,
    });
  };

  updateTodo = (id, task) => {
    this.setState(({ todos }) => {
      todos
        .filter((todo) => todo._id === id)
        .map((todo) => {
          todo.task = task;
        });
      return todos;
    });
  };

  componentDidMount() {
    const todos = getTodos() || [];
    this.setState({ todos });
  }

  componentDidUpdate(previousProps, previouState) {
    const todos = this.state.todos;
    setTodos(todos);
  }

  render() {
    const {
      saveTodo,
      changeCheckedAllTodos,
      deleteTodo,
      changeCheckedTodo,
      deleteCompleted,
      updateTodo,
    } = this;
    const { status, todos } = this.state;
    return (
      <React.Fragment>
        <h1>todos</h1>
        <div className="container">
          <Header saveTodo={saveTodo} />
          <Main
            status={status}
            todos={todos}
            onDelete={deleteTodo}
            toggleTodo={changeCheckedTodo}
            toggleAllTodos={changeCheckedAllTodos}
            updateTodo={updateTodo}
          />
          <Footer
            activeTodos={
              todos.filter((todo) => todo["isCompleted"] === false).length
            }
            completedTodos={
              todos.filter((todo) => todo["isCompleted"] === true).length
            }
            deleteCompletedHandler={deleteCompleted}
          />
        </div>
      </React.Fragment>
    );
  }
}

export default index;
