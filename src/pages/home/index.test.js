import React from "react";
import { render, fireEvent, screen, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom";

import Home from "./index";
import { getTodos } from "../../service";

afterEach(cleanup);

test("Input should add a new item in the list", () => {
  expect.assertions(3);
  const enteredValue = "sample text";
  const { container } = render(<Home status="all" />);
  expect(container.querySelector(".list")).toBeEmptyDOMElement();
  fireEvent.change(container.querySelector("input.mainInput"), {
    target: {
      value: enteredValue,
    },
  });
  fireEvent.submit(container.querySelector("form"));
  expect(screen.getByTestId("listItem").textContent).toBe(enteredValue);
  expect(getTodos().length).toBe(1);
});

test("CheckBox should work properly", () => {
  const { container } = render(<Home status="all" />);
  fireEvent.click(container.querySelector(".toggle"));
  expect(getTodos()[0].isCompleted).toBeTruthy();
});

test("All checkBox should work as expected", () => {
  const { container } = render(<Home status="all" />);
  fireEvent.click(container.querySelector("input.allCheckBox"));
  expect(getTodos()[0].isCompleted).toBeFalsy();
});

test("Item should update and call updateTodo", () => {
  const editedTask = "sample edited task";
  const { container } = render(<Home status="all" />);
  fireEvent.doubleClick(screen.getByTestId("listItemTask"));
  fireEvent.change(container.querySelector("input.editForm"), {
    target: {
      value: editedTask,
    },
  });
  expect(screen.getByTestId("listItem")).toHaveClass("hide");
  fireEvent.submit(screen.getByTestId("editInputForm"));
  expect(screen.getByTestId("listItem").textContent).toBe(editedTask);
});

test("Delete button should work", () => {
  expect.assertions(2);
  const { container } = render(<Home status="all" />);
  fireEvent.click(screen.getByTestId("deleteButton"));
  expect(container.querySelector(".list")).toBeEmptyDOMElement();
  expect(getTodos().length).toBe(0);
});

test("Completed Delete button should work", () => {
  const { container } = render(<Home status="all" />);
  fireEvent.change(container.querySelector("input.mainInput"), {
    target: {
      value: "sample text",
    },
  });
  fireEvent.submit(container.querySelector("form"));
  expect(getTodos().length).toBe(1);
  fireEvent.click(container.querySelector(".toggle"));
  fireEvent.click(container.querySelector(".clearCompleted"));
  expect(container.querySelector(".list")).toBeEmptyDOMElement();
  expect(getTodos().length).toBe(0);
});
