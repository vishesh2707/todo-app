import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";

import Button from "./index";

test("Button should render and call the provided onClickHandler", () => {
  expect.assertions(3);
  const mockedOnClickHandler = jest.fn((event) => {
    expect(event.target.className).toBe("deleteButton");
  });

  const { container } = render(
    <Button onClickHandler={mockedOnClickHandler} />
  );
  expect(container.querySelector("button.deleteButton")).toBeVisible();
  fireEvent.click(container.querySelector("button.deleteButton"));
  expect(mockedOnClickHandler).toBeCalledTimes(1);
});
