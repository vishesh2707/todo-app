import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";

import Input from "./index";

test("Input should'nt call saveTodo with empty text", () => {
  expect.assertions(2);
  const enteredValue = "";
  const mockedSaveTodo = jest.fn((task) => {
    expect(task).toBe(enteredValue);
  });

  const { container } = render(<Input saveTodo={mockedSaveTodo} />);
  expect(container.querySelector("input.mainInput")).toBeVisible();
  fireEvent.change(container.querySelector("input.mainInput"), {
    target: {
      value: enteredValue,
    },
  });
  fireEvent.submit(container.querySelector("form"));
  expect(mockedSaveTodo).toBeCalledTimes(0);
});

test("Input should call saveTodo with entered text", () => {
  expect.assertions(3);
  const enteredValue = "sample text";
  const mockedSaveTodo = jest.fn((task) => {
    expect(task).toBe(enteredValue);
  });

  const { container } = render(<Input saveTodo={mockedSaveTodo} />);
  expect(container.querySelector("input.mainInput")).toBeVisible();
  fireEvent.change(container.querySelector("input.mainInput"), {
    target: {
      value: enteredValue,
    },
  });
  fireEvent.submit(container.querySelector("form"));
  expect(mockedSaveTodo).toBeCalledTimes(1);
});
