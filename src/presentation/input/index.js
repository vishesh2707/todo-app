import React, { Component } from "react";

class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      task: "",
    };
  }

  onChangeHandler = (event) => {
    let inputTask = event.target.value;
    this.setState({ task: inputTask });
  };

  onSubmitHandler = (event) => {
    event.preventDefault();
    const trimmedInput = this.state.task.trim();
    if (trimmedInput !== "") {
      this.props.saveTodo(trimmedInput);
      this.setState({ task: "" });
    }
  };

  render() {
    return (
      <form onSubmit={this.onSubmitHandler}>
        <input
          type="text"
          className="mainInput"
          value={this.state.task}
          placeholder="What needs to be done?"
          onChange={this.onChangeHandler}
          autoFocus
        />
      </form>
    );
  }
}

export default index;
