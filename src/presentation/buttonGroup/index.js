import React from "react";
import { NavLink, HashRouter as Router } from "react-router-dom";

import "./style.css";

function index() {
  return (
    <Router>
      <div className="navLinks">
        <NavLink data-testid="all" exact activeClassName="selected" to="/">
          All
        </NavLink>
        <NavLink data-testid="active" activeClassName="selected" to="/active">
          Active
        </NavLink>
        <NavLink
          data-testid="completed"
          activeClassName="selected"
          to="/completed"
        >
          Completed
        </NavLink>
      </div>
    </Router>
  );
}

export default index;
