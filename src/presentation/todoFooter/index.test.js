import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";

import Footer from "./index";

test("Footer should render conditionally", () => {
  expect.assertions(3);
  const activeTodos = 0;
  const completedTodos = 1;
  const mockedOnDeleteHandler = jest.fn(() => {
    rerender(
      <Footer
        activeTodos={activeTodos}
        completedTodos={completedTodos - 1}
        deleteCompletedHandler={mockedOnDeleteHandler}
      />
    );
  });
  const { container, rerender } = render(
    <Footer
      activeTodos={activeTodos}
      completedTodos={completedTodos}
      deleteCompletedHandler={mockedOnDeleteHandler}
    />
  );
  expect(container.querySelector(".footer")).toBeInTheDocument();
  expect(container.querySelector(".footer p").textContent).toBe(
    activeTodos + " items left"
  );
  fireEvent.click(container.querySelector(".clearCompleted"));
  expect(container.querySelector(".footer")).not.toBeInTheDocument();
});
