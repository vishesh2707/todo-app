import React from "react";
import { HashRouter as Router } from "react-router-dom";

import NavLinks from "../buttonGroup";
import "./style.css";

function index({ activeTodos, completedTodos, deleteCompletedHandler }) {
  if (activeTodos > 0 || completedTodos > 0) {
    return (
      <Router>
        <div className="footer">
          <p>
            {activeTodos} {activeTodos === 1 ? "item left" : "items left"}
          </p>
          <NavLinks />
          <button className="clearCompleted" onClick={deleteCompletedHandler}>
            Clear Completed
          </button>
        </div>
      </Router>
    );
  } else {
    return null;
  }
}

export default index;
