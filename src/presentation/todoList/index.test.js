import React from "react";
import { render, fireEvent, screen, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom";

import TodosList from "./index";

afterEach(cleanup);

test("Input checkbox and todos-list should change conditionally", () => {
  expect.assertions(6);
  const todos = [
    { id: 1, task: "sample task 1", isCompleted: true },
    { id: 2, task: "sample task 2", isCompleted: true },
    { id: 3, task: "sample task 3", isCompleted: true },
  ];
  const status = "all";
  const mockedToggleAllTodos = jest.fn((isCompleted) => {
    expect(isCompleted).toBe(true);
    rerender(
      <TodosList
        todos={[]}
        status={status}
        onDelete={() => {}}
        toggleTodo={() => {}}
        toggleAllTodos={mockedToggleAllTodos}
        updateTodo={() => {}}
      />
    );
  });

  const { container, rerender } = render(
    <TodosList
      todos={todos}
      status={status}
      onDelete={() => {}}
      toggleTodo={() => {}}
      toggleAllTodos={mockedToggleAllTodos}
      updateTodo={() => {}}
    />
  );

  expect(container.querySelector("input.allCheckBox")).toBeVisible();
  expect(container.querySelector("ul.list")).toBeVisible();
  expect(container.querySelector("input.allCheckBox")).toBeChecked();
  fireEvent.click(container.querySelector("input.allCheckBox"));
  expect(container.querySelector("input.allCheckBox")).not.toBeInTheDocument();
  expect(container.querySelector("ul.list")).toBeEmptyDOMElement();
});

test("List should be display according to status", () => {
  expect.assertions(4);
  const todos = [
    { id: 1, task: "sample task 1", isCompleted: true },
    { id: 2, task: "sample task 2", isCompleted: false },
    { id: 3, task: "sample task 3", isCompleted: true },
  ];

  let { container } = render(
    <TodosList
      todos={todos}
      status="active"
      onDelete={() => {}}
      toggleTodo={() => {}}
      toggleAllTodos={() => {}}
      updateTodo={() => {}}
    />
  );

  expect(container.querySelector("ul.list")).toBeVisible();
  expect(screen.getByTestId("listItem").textContent).toBe("sample task 2");
  cleanup();
  container = render(
    <TodosList
      todos={todos}
      status="completed"
      onDelete={() => {}}
      toggleTodo={() => {}}
      toggleAllTodos={() => {}}
      updateTodo={() => {}}
    />
  ).container;
  expect(container.querySelector("ul.list")).toBeVisible();
  expect(screen.getAllByTestId("listItem").length).toBe(2);
});
