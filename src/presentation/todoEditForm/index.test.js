import React from "react";
import { render, fireEvent, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom";

import EditFormInput from "./index";

afterEach(cleanup);

test("Edit Input should'nt call updateTodo with empty text", () => {
  expect.assertions(4);
  const enteredValue = "sample edit form text";
  const changedEnteredValue = "";
  const mockedUpdateTodo = jest.fn((task) => {
    expect(task).toBe(changedEnteredValue);
  });
  const mockedCancelTodo = jest.fn(() => {
    rerender(
      <EditFormInput
        task={enteredValue}
        isEditing={false}
        updateTodo={mockedUpdateTodo}
        cancelEditing={mockedCancelTodo}
      />
    );
  });

  const { container, rerender } = render(
    <EditFormInput
      task={enteredValue}
      isEditing={true}
      updateTodo={mockedUpdateTodo}
      cancelEditing={mockedCancelTodo}
    />
  );
  expect(container.querySelector("input.editForm")).toBeVisible();
  expect(container.querySelector("input.editForm").value).toBe(enteredValue);
  fireEvent.change(container.querySelector("input.editForm"), {
    target: {
      value: changedEnteredValue,
    },
  });
  fireEvent.submit(container.querySelector("form"));
  expect(mockedUpdateTodo).toBeCalledTimes(0);
  expect(container.querySelector("input.editForm")).not.toBeVisible();
});

test("Edit Input should call updateTodo with entered text", () => {
  expect.assertions(5);
  const enteredValue = "sample edit form text";
  const changedEnteredValue = "changed sample edit form text";
  const mockedUpdateTodo = jest.fn((task) => {
    expect(task).toBe(changedEnteredValue);
  });
  const mockedCancelTodo = jest.fn(() => {
    rerender(
      <EditFormInput
        task={enteredValue}
        isEditing={false}
        updateTodo={mockedUpdateTodo}
        cancelEditing={mockedCancelTodo}
      />
    );
  });

  const { container, rerender } = render(
    <EditFormInput
      task={enteredValue}
      isEditing={true}
      updateTodo={mockedUpdateTodo}
      cancelEditing={mockedCancelTodo}
    />
  );
  expect(container.querySelector("input.editForm")).toBeVisible();
  expect(container.querySelector("input.editForm").value).toBe(enteredValue);
  fireEvent.change(container.querySelector("input.editForm"), {
    target: {
      value: changedEnteredValue,
    },
  });
  fireEvent.submit(container.querySelector("form"));
  expect(mockedUpdateTodo).toBeCalledTimes(1);
  expect(container.querySelector("input.editForm")).not.toBeVisible();
});

test("Edit Input should'nt be cancelled on pressing any key except Escape button", () => {
  expect.assertions(4);
  const enteredValue = "sample edit form text";
  const mockedCancelTodo = jest.fn(() => {
    rerender(
      <EditFormInput
        task={enteredValue}
        isEditing={false}
        updateTodo={() => {}}
        cancelEditing={mockedCancelTodo}
      />
    );
  });

  const { container, rerender } = render(
    <EditFormInput
      task={enteredValue}
      isEditing={true}
      updateTodo={() => {}}
      cancelEditing={mockedCancelTodo}
    />
  );
  expect(container.querySelector("input.editForm")).toBeVisible();
  expect(container.querySelector("input.editForm").value).toBe(enteredValue);
  fireEvent.keyDown(container.querySelector("input.editForm"), {
    keyCode: 28,
  });
  expect(mockedCancelTodo).toBeCalledTimes(0);
  expect(container.querySelector("input.editForm")).toBeVisible();
});

test("Edit Input should be cancelled on pressing Escape button", () => {
  expect.assertions(4);
  const enteredValue = "sample edit form text";
  const mockedCancelTodo = jest.fn(() => {
    rerender(
      <EditFormInput
        task={enteredValue}
        isEditing={false}
        updateTodo={() => {}}
        cancelEditing={mockedCancelTodo}
      />
    );
  });

  const { container, rerender } = render(
    <EditFormInput
      task={enteredValue}
      isEditing={true}
      updateTodo={() => {}}
      cancelEditing={mockedCancelTodo}
    />
  );
  expect(container.querySelector("input.editForm")).toBeVisible();
  expect(container.querySelector("input.editForm").value).toBe(enteredValue);
  fireEvent.keyDown(container.querySelector("input.editForm"), {
    keyCode: 27,
  });
  expect(mockedCancelTodo).toBeCalledTimes(1);
  expect(container.querySelector("input.editForm")).not.toBeVisible();
});
