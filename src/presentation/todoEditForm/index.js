import React, { Component } from "react";

import "./style.css";

export class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      task: this.props.task,
    };
  }

  handleChange = (event) => {
    const task = event.target.value;
    this.setState({ task });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const { task } = this.state;

    if (task) {
      this.props.updateTodo(task);
    }
    this.props.cancelEditing();
  };

  handleEscape = (event) => {
    if (event.which === 27) {
      this.props.cancelEditing();
    }
  };

  componentDidUpdate() {
    this.input.focus();
  }

  render() {
    const { task } = this.state;
    return (
      <form data-testid="editInputForm" onSubmit={this.handleSubmit}>
        <input
          ref={(input) => (this.input = input)}
          type="text"
          style={
            this.props.isEditing ? { display: "block" } : { display: "none" }
          }
          value={task}
          className="editForm"
          onBlur={this.handleSubmit}
          onChange={this.handleChange}
          onKeyDown={this.handleEscape}
        />
      </form>
    );
  }
}

export default index;
