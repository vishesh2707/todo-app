import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";

import CheckBox from "./index";

test("CheckBox should be checked conditionally and fire given callback when changed", () => {
  expect.assertions(3);
  const mockedOnChangeHandler = jest.fn(() => {
    rerender(
      <CheckBox onChangeHandler={mockedOnChangeHandler} isCompleted={false} />
    );
  });

  const { container, rerender } = render(
    <CheckBox onChangeHandler={mockedOnChangeHandler} isCompleted={true} />
  );
  expect(container.querySelector("input.toggle")).toBeVisible();
  expect(container.querySelector("input.toggle").checked).toBeTruthy();
  fireEvent.click(container.querySelector("input.toggle"));
  expect(container.querySelector("input.toggle").checked).toBeFalsy();
});
