import React from "react";

import "./style.css";

function index({ onChangeHandler, isCompleted }) {
  return (
    <input
      type="checkbox"
      className="toggle"
      checked={isCompleted}
      onChange={onChangeHandler}
    />
  );
}

export default index;
