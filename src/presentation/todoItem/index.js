import React, { useState } from "react";

import TodoEditForm from "../todoEditForm";
import CheckBox from "../checkBox";
import DeleteButton from "../button";

import "./style.css";

function index({ id, task, onDelete, toggleTodo, isCompleted, updateTodo }) {
  const [editing, setEditing] = useState(null);
  const editTodo = (providedID) => {
    setEditing(providedID);
  };
  const cancelEditing = () => {
    setEditing(null);
  };
  return (
    <li key={id}>
      <div
        data-testid="listItem"
        className={editing === id ? "hide" : "labelDiv"}
      >
        <CheckBox
          isCompleted={isCompleted && 1}
          onChangeHandler={() => toggleTodo(id)}
        />
        <span
          data-testid="listItemTask"
          onDoubleClick={() => editTodo(id)}
          className={isCompleted ? "completed" : ""}
        >
          {task}
        </span>
        <DeleteButton onClickHandler={() => onDelete(id)} />
      </div>
      <TodoEditForm
        task={task}
        isEditing={editing && true}
        updateTodo={(task) => updateTodo(id, task)}
        cancelEditing={() => cancelEditing()}
      />
    </li>
  );
}

export default index;
