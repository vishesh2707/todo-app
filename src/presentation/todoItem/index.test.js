import React from "react";
import { render, fireEvent, screen, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom";

import ListItem from "./index";

afterEach(cleanup);

test("ListItem should toggle between editing and display mode", () => {
  expect.assertions(2);
  const id = "sample ID";
  const task = "sample task";

  const { container } = render(
    <ListItem
      id={id}
      task={task}
      onDelete={() => {}}
      toggleTodo={() => {}}
      isCompleted={() => {}}
      updateTodo={() => {}}
    />
  );
  fireEvent.doubleClick(screen.getByTestId("listItemTask"));
  expect(screen.getByTestId("listItem")).toHaveClass("hide");
  fireEvent.keyDown(container.querySelector("input.editForm"), {
    keyCode: 27,
  });
  expect(screen.getByTestId("listItem")).toHaveClass("labelDiv");
});

test("ListItem should call update todo with correct id and task", () => {
  expect.assertions(4);
  const id = "sample ID";
  const task = "sample task";
  const editedTask = "sample edited task";

  const mockedUpdateTodo = jest.fn((receivedId, task) => {
    expect(task).toBe(editedTask);
    expect(receivedId).toBe(id);
  });

  const { container } = render(
    <ListItem
      id={id}
      task={task}
      onDelete={() => {}}
      toggleTodo={() => {}}
      isCompleted={() => {}}
      updateTodo={mockedUpdateTodo}
    />
  );
  fireEvent.doubleClick(screen.getByTestId("listItemTask"));
  expect(screen.getByTestId("listItem")).toHaveClass("hide");
  fireEvent.change(container.querySelector("input.editForm"), {
    target: {
      value: editedTask,
    },
  });
  fireEvent.submit(container.querySelector("form"));
  expect(screen.getByTestId("listItem")).toHaveClass("labelDiv");
});
